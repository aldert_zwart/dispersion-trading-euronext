# -*- coding: utf-8 -*-
"""
Created on Thu Feb 24 14:17:04 2022

@author: ajzwa


"""
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import numpy as np
import matplotlib.dates as mdates
import datetime

class VolatilityCorrelation:
    """
    The class VolatilityCorrelation calculates the volatility of the index & the tracking portfolio.
       
    """
       
    def rolling_vol_index(self, data, window):
        mvol = data.rolling(window=window).var()
        mvol = mvol.dropna(axis=0)
        mvol = mvol.squeeze()
        mvol.index = pd.to_datetime(mvol.index)
        return mvol

    def rolling_vol_components(self, data,window,weights):
        mvol = (weights.tolist()*data.rolling(window=window).var()).dropna(axis=0).sum(axis=1)
        mvol.index = pd.to_datetime(mvol.index)
        return mvol
    
    
    def implied_corr(self, index, components, window, weights):
        index_vol = index.rolling(window=window).var().dropna(axis=0)
        weighted_vol = (weights.tolist()*components.rolling(window=window).var()).dropna(axis=0).sum(axis=1)
        
        
    def dispersion(self, index, portfolio, weights, window):
        mvol_index = index.rolling(window=window).var().dropna(axis=0).squeeze()
        mvol_portfolio = (weights.tolist()*portfolio.rolling(window=window).var()).dropna(axis=0).sum(axis=1)
        dispersion = mvol_portfolio-mvol_index
        return dispersion 

    def implied_correlation(self,index, components, weights, window):
        mvol_index = index.rolling(window=window).var().dropna(axis=0).squeeze()
        mvol_portfolio = components.rolling(window=window).var().dropna(axis=0)
        p = (weights.tolist()*mvol_portfolio).sum(axis=1)
        dispersion = p-mvol_index
        wi = weights
        wj = weights.transpose()
        sigmai = mvol_portfolio
        for row in mvol_portfolio.iterrows():
            print(row)
            #implied_cor = wi*wj*row*row.transpose()
            return print(row)

    def implied_corr(self,index, portfolio,weights):
        wi = weights
        wj = weights.transpose()
        wi2=(weights**2).tolist()
        var_index = index.var().dropna(axis=0)
        var_portfolio = portfolio.var().dropna(axis=0)
        cov = portfolio.cov().dropna(axis=0)
        x = np.dot(wj,np.dot(cov,wi))
        implied_cor=(var_index-sum(wi2*var_portfolio))/2*x
        return implied_cor
        
        