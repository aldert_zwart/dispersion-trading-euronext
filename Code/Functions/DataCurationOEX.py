""""
The datacuration extracts returns from the components of the RIC registration from the files "components"
"""

import yfinance as yf
import pandas as pd
import datetime
import time

class DataCuration:
    """
    the class DataCuration retrieves returns from an given list
       
    Attributes
    ----------
        start_date: date
        end_date: date

    Methods
    ----------
        returns_df: gives a dataframe with returns
    """
    

    def returns_components_df(self, start_date, end_date):
        """
        the returns_components_df method returns a PandasDataframe contain 
        the returns given for a period. 

        Parameters
        -----------
        begin_date
        end_date
  
        Returns
        --------
        pandas DataFrame
        """
        comp=pd.read_csv("../Data/components/components_oex.csv", header=0)
        df_merged = pd.DataFrame()
        for ticker in comp.Instrument:
            ticker = ticker.split(".",1)
            info = yf.Ticker(str(ticker[0]))
            df_individual = info.history(start=start_date, end=end_date)
            df_merged[ticker[0]]=df_individual.Close
        return df_merged
        
    def returns_index_df(self, start_date, end_date):
        """
        the returns_components_df method returns a PandasDataframe contain the returns given for a period. 

        Parameters
        -----------
        begin_date
        end_date
  
        Returns
        --------
        pandas DataFrame
        """
        info = yf.Ticker("^OEX")
        df = info.history(start=start_date, end=end_date)
        df_aex = df.Close
        return df_aex
    
    def meta_data(self):
        """
        Extractes meta data from the components with the help of the yahoo finance API

        Parameters
        -----------

        Returns
        --------
        writes a csv file to the data folders
        """
        comp=pd.read_csv("../Data/components/components_oex.csv", header=0)
        final_df = pd.DataFrame()
        append = []
        for ticker in comp.Instrument:            
            _ = ticker.split(".",1)
            _ = yf.Ticker(str(_[0]))
            info = _.info
            df = pd.DataFrame([info])
            df['ticker']=ticker
            append.append(df)
            time.sleep(15)
        final_df = pd.concat(append)
        final_df.to_csv("../Data/components/metadata_oex.csv")
        return print("Meta Data is saved at location: ../Data/components/metadata_oex.csv")

    def cleansing_data(self,data):
        """
        Cleansed the data including missing values and IPOs

        Parameters
        -----------

        Returns
        --------
        overwrites the csv file to the data folders
        """
        df=data
        first_row = df.iloc[:1]
        columns_to_drop=first_row.columns[first_row.isna().all(0)]
        df = df.drop(columns_to_drop, axis=1)
        df = df.fillna(method='bfill')
        df.to_csv("../data/price/price_components.csv")
        return df

        