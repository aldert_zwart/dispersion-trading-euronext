# -*- coding: utf-8 -*-
"""
Created on Fri Apr 15 18:38:51 2022

@author: ajzwa
"""
import yfinance as yf
import pandas as pd
import datetime
import time
from sklearn.decomposition import PCA
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from scipy import stats
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import matplotlib.dates as mdates
import datetime

class DataCuration:
    """
    the class DataCuration retrieves returns from an given list
       
    Attributes
    ----------
        start_date: date
        end_date: date

    Methods
    ----------
        returns_df: gives a dataframe with returns
    """
    

    def returns_components_df(self, start_date, end_date):
        """
        the returns_components_df method returns a PandasDataframe contain 
        the returns given for a period. 

        Parameters
        -----------
        begin_date
        end_date
  
        Returns
        --------
        pandas DataFrame
        """
        comp=pd.read_csv("../Data/components/components_oex.csv", header=0)
        df_merged = pd.DataFrame()
        for ticker in comp.Instrument:
            ticker = ticker.split(".",1)
            info = yf.Ticker(str(ticker[0]))
            df_individual = info.history(start=start_date, end=end_date)
            df_merged[ticker[0]]=df_individual.Close
        return df_merged
        
    def returns_index_df(self, start_date, end_date):
        """
        the returns_components_df method returns a PandasDataframe contain the returns given for a period. 

        Parameters
        -----------
        begin_date
        end_date
  
        Returns
        --------
        pandas DataFrame
        """
        info = yf.Ticker("^OEX")
        df = info.history(start=start_date, end=end_date)
        df_aex = df.Close
        return df_aex
    
    def meta_data(self):
        """
        Extractes meta data from the components with the help of the yahoo finance API

        Parameters
        -----------

        Returns
        --------
        writes a csv file to the data folders
        """
        comp=pd.read_csv("../Data/components/components_oex.csv", header=0)
        final_df = pd.DataFrame()
        append = []
        for ticker in comp.Instrument:            
            _ = ticker.split(".",1)
            _ = yf.Ticker(str(_[0]))
            info = _.info
            df = pd.DataFrame([info])
            df['ticker']=ticker
            append.append(df)
            time.sleep(15)
        final_df = pd.concat(append)
        final_df.to_csv("../Data/components/metadata_oex.csv")
        return print("Meta Data is saved at location: ../Data/components/metadata_oex.csv")

    def cleansing_data(self,data):
        """
        Cleansed the data including missing values and IPOs

        Parameters
        -----------

        Returns
        --------
        overwrites the csv file to the data folders
        """
        df=data
        first_row = df.iloc[:1]
        columns_to_drop=first_row.columns[first_row.isna().all(0)]
        df = df.drop(columns_to_drop, axis=1)
        df = df.fillna(method='bfill')
        df.to_csv("../data/price/price_components.csv")
        return df

class PortfolioTracking:
    """
    the class PortfolioTracking mimics an index with a N number of its components
       
    Attributes
    ----------
        returns components
        returns index

    Methods
    ----------
        weights_df: gives a dataframe with weights of the tracking portfolio
    """
    def pca_plot(self):
        """
        calculates the principle component analysis of the covariance matrix of the index components
        
        Parameters
        -----------
        n_components, data
          
        Returns
        --------
        scatter plat with the pca analysis including the sector
        """
                
        p_components = pd.read_csv("../../Data/price/constituents_oex_cleansed.csv",index_col=0)
        meta_data = pd.read_csv("../../data/components/metadata_oex.csv")
        #calculate the returns from the price
        returns_components = p_components.pct_change().dropna()
        returns_components = returns_components.cov()        
        #apply a 2 dimensional pca in order to able to plot a 2 dimensional graph with the results
        n_components=3  
        #perform the PCA
        X = returns_components.transpose()
        
        pca = PCA(n_components=n_components)
        components = pca.fit_transform(X) 
        df_components = pd.DataFrame(data=components,columns=['x', 'y', 'z'])
        df_components = df_components.set_index(returns_components.columns)
        #add meta data regarding the sector and marketCap
        
        meta_data=meta_data.set_index(meta_data.symbol)
        df_components = pd.merge(df_components, meta_data.sector, left_index=True, right_index=True)

        #group by sector in order to including the sectors in the graph
        groups = df_components.groupby('sector')
        # Plot
        sns.set(style = "darkgrid")
        
        fig = plt.figure(figsize=(10,14))
        ax = fig.add_subplot(projection = '3d')
        for name, group in groups:
            ax.plot(group.x, group.y, group.z, marker='o', linestyle='', ms=12, label=name, alpha=0.75)
        ax.legend(bbox_to_anchor=(1.35, 1))
        plt.title("Principle Component Analysis OEX")
        plt.savefig("../../Graphs/pca_oex.png", bbox_extra_artists=(ax,), bbox_inches='tight')
        return plt
    
    def pca_tracking_weights(self):
        """
        calculates the weights for every component in order to track the index
        
        Parameters
        -----------

          
        Returns
        --------
        DataFrame with the name & weights
        """
        #import the data
        p_components = pd.read_csv("../../Data/price/constituents_oex_cleansed.csv",index_col=0)
        meta_data = pd.read_csv("../../data/components/metadata_oex.csv")
        #calculate the returns from the price
        returns_components = p_components.pct_change().dropna()
        returns_components = returns_components.cov()        
        #apply a 2 dimensional pca in order to able to plot a 2 dimensional graph with the results
        n_components=2  
        #perform the PCA
        X = returns_components.transpose()
        pca = PCA(n_components=n_components)
        components = pca.fit_transform(X) 
        df_components = pd.DataFrame(data=components,columns=['x', 'y'])
        df_components = df_components.set_index(returns_components.columns)
        #add meta data regarding the sector and marketCap
        
        meta_data=meta_data.set_index(meta_data.symbol)
        df_components = pd.merge(df_components, meta_data.ticker, left_index=True, right_index=True)
        #calculate the weights with the help of the explained variance
        weights = abs(df_components['x'])/sum(abs(df_components['x']))
        df_components['weights'] = weights.tolist()
        return df_components[['ticker',"weights"]]

        
    def pca_tracking_plot(self):
        """
        plots the tracking index and index and calculates the MSE
        
        Parameters
        -----------

          
        Returns
        --------
        mean squared error of the index and tracking index & plot with the returns of both the index & tracking index
        """
        #import the data
        data = pd.read_csv("../../Data/price/constituents_oex_cleansed.csv", index_col=0)
        meta_data = pd.read_csv("../../Data/components/metadata_oex.csv")
        p_index = pd.read_csv("../../Data/price/price_oexYF.csv", index_col=0)
        #calculate the returns
        returns_components = data.pct_change().dropna() 
        returns_index=p_index.pct_change().dropna()
        returns_components_cov = returns_components.cov()  
        #perfrom the pca
        X = returns_components_cov.transpose()
        pca = PCA(n_components=2)
        components = pca.fit_transform(X)
        df_components = pd.DataFrame(data=components,columns=['x', 'y'])
        df_components = df_components.set_index(returns_components.columns)
        #add meta data regarding the sector and marketCap
        
        meta_data=meta_data.set_index(meta_data.symbol)
        df_components = pd.merge(df_components, meta_data.ticker, left_index=True, right_index=True)
        
        #calculate the weights
        weights = abs(df_components['x'])/sum(abs(df_components['x']))
        df_components['weights'] = weights.tolist()
        tracking_index = weights.tolist()*returns_components
        portfolio_index = tracking_index.sum(axis=1)
        
        index_returns = returns_index[returns_index.index.isin(portfolio_index.index)]
        
        fig, ax = plt.subplots(figsize=(12,6))
        plt.title("Returns Tracking Index")
        ax.plot(portfolio_index,label='tracking portfolio')
        ax.plot(index_returns,label='index')
        ax.xaxis.set_ticklabels([])
        ax.legend() 
        #calculated MSE & t-test
        mse = mean_squared_error(index_returns,portfolio_index)
        plt.savefig("../../Graphs/portfolio_index_returns.png")
        return(print("Mean Sqaured error:",mse),plt.show())

    def cum_returns(self):
        """
        
        
        Parameters
        -----------

          
        Returns
        --------
        
        """
        #import the data
        data = pd.read_csv("../../Data/price/constituents_oex_cleansed.csv", index_col=0)
        meta_data = pd.read_csv("../../Data/components/metadata_oex.csv")
        p_index = pd.read_csv("../../Data/price/price_oexYF.csv", index_col=0)
        #calculate the returns
        returns_components = data.pct_change().dropna() 
        returns_index=p_index.pct_change().dropna()
        returns_components_cov = returns_components.cov() 
        #perfrom the pca
        X = returns_components_cov.transpose()
        pca = PCA(n_components=2)
        components = pca.fit_transform(X)
        df_components = pd.DataFrame(data=components,columns=['x', 'y'])
        df_components["Name"]=meta_data.shortName
        #calculate the weights
        weights = abs(df_components['x'])/sum(abs(df_components['x']))
        df_components['weights'] = weights.tolist()
        tracking_index = weights.tolist()*returns_components
        portfolio_index = tracking_index.sum(axis=1)
        
        index_returns = returns_index[returns_index.index.isin(portfolio_index.index)]
        cum_index = (index_returns +1).cumprod()
        cum_portfolio = (portfolio_index+1).cumprod()
        
        fig, ax = plt.subplots(figsize=(12,6))
        plt.title("Cumulative Returns Tracking Portfolio & Index")
        ax.plot(cum_portfolio,label='tracking portfolio')
        ax.plot(cum_index,label='index')
        ax.xaxis.set_ticklabels([])
        ax.legend()     
        plt.savefig("../../Graphs/cum_returns_index_portfolio.png")
        t_test  = stats.ttest_ind(cum_portfolio,cum_index)

        return plt.show(), print("T-test",t_test)
    
    def top_n(self):
        """
        """
        #import the data
        data = pd.read_csv("../../Data/price/constituents_oex_cleansed.csv", index_col=0)
        meta_data = pd.read_csv("../../Data/components/metadata_oex.csv")
        #calculate the returns
        returns_components = data.pct_change().dropna() 
        returns_components_cov = returns_components.cov()  
        #perfrom the pca
        n_components=7
        X = returns_components_cov.transpose()
        pca = PCA(n_components=n_components)
        components = pca.fit_transform(X)
        joint_explaination = sum(pca.explained_variance_ratio_)
        
        weighted_arithmetic_mean= pca.explained_variance_ratio_/joint_explaination
        df_components = pd.DataFrame(data=components)
        df_components["Name"]=meta_data.ticker
        weighted_mean_corr = pd.DataFrame(
            data=[[np.corrcoef(returns_components_cov[c],
                               components[:,n])[1,0] for n in range(pca.n_components_)] 
                  for c in returns_components_cov], index=returns_components_cov.columns)
        
        df = ((weighted_mean_corr**2)*weighted_arithmetic_mean).sum(axis=1)
        df = df.sort_values(ascending=False)
        df = df.nlargest(40)
        return df

      
class ProfitLost:
    """
    The class ProfitLost caclulates the PnL of the dispersion trading strategy
    """
                  
        
    def trade_book_plain(self,enter, option_data, df_weights, df_price,date_lower_bound, date_higher_bound,list_symbols):
        """

        Parameters
        ----------
        enter : TYPE
            DESCRIPTION.
        option_data : TYPE
            DESCRIPTION.
        df_weights : TYPE
            DESCRIPTION.
        df_price : TYPE
            DESCRIPTION.
        date_lower_bound : TYPE
            DESCRIPTION.
        date_higher_bound : TYPE
            DESCRIPTION.
        list_symbols : TYPE
            DESCRIPTION.

        Returns
        -------
        trade_book_plain : TYPE
            DESCRIPTION.

        """
        trade_book_plain = pd.DataFrame()
        for row in range(len(enter)):
            for ticker in list_symbols:
                option_data_ticker = option_data[option_data.ticker==ticker] #get data of the ticker
                close_price_group = df_price.loc[enter.iloc[row].name] #get the close price of the group of asset for the strike price 
                close_price = close_price_group.loc[close_price_group.index == ticker] #get the close price for the individual asset            
                df_filtered_date=option_data_ticker.loc[option_data_ticker.date==enter.iloc[row].name]
                df_filtered_strike=df_filtered_date.loc[df_filtered_date.strike_price==int(round(close_price,-1)*1000)]
                df_filtered_maturity=df_filtered_strike.loc[(df_filtered_strike.exdate>enter.iloc[row].name+timedelta(days=date_lower_bound)) ]
                #strategy for the index
                if ticker == 'OEX':
                    if enter.iloc[row].trade == 'Short':
                        #Short dispersion is buy OEX calls
                        df_filtered_cp =df_filtered_maturity.loc[df_filtered_maturity.cp_flag=='C']
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short                    
                        trade_book_plain=pd.concat([df_filtered_cp,trade_book_plain],ignore_index=True)
    
                        
                    if enter.iloc[row].trade == 'Long':
                        #long dispersion is buy OEX puts
                        df_filtered_cp=df_filtered_maturity.loc[df_filtered_maturity.cp_flag=='P']
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_plain=pd.concat([df_filtered_cp,trade_book_plain],ignore_index=True)
                        
                else:
                    if enter.iloc[row].trade == 'Short':
                        #Short dispersion is buy consituents puts
                        df_filtered_cp =df_filtered_maturity.loc[df_filtered_maturity.cp_flag=='P']
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_plain=pd.concat([df_filtered_cp,trade_book_plain],ignore_index=True)
    
                        
                    if enter.iloc[row].trade == 'Long':
                        #long dispersion is buy constituents calls
                        df_filtered_cp =df_filtered_maturity.loc[df_filtered_maturity.cp_flag=='C']
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_plain=pd.concat([df_filtered_cp,trade_book_plain],ignore_index=True)
    
                        
        return trade_book_plain
    
    def get_exit_date_opposite_trade(self, enter):
        """
        

        Parameters
        ----------
        enter : TYPE
            DESCRIPTION.

        Returns
        -------
        df_exit_date : TYPE
            DESCRIPTION.

        """

        change=[]
        df_exit_date=pd.DataFrame()
        i=0
        exit_date=[]
        
        for row in range(len(enter)):
            try:
                if enter.iloc[row].trade != enter.iloc[row+1].trade:
                    change.append(enter.iloc[row+1].name)
            except:
                print('End of Loop')
        
 
        try:
            for row in range(len(enter)):
                exit_date.append(change[i])
                for row2 in range(len(change)):
                    if enter.iloc[row].name==change[row2]:
                        i+=1
        
        except:
            print()
        exit_date.pop(0)
        difference = len(enter)-len(exit_date)
        for _ in range(difference):
            exit_date.append(np.nan)
        
        df_exit_date=enter
        df_exit_date['exit_date']=exit_date 
        return df_exit_date
    
    def returns_portfolio(self, df_portfolio, enter, list_symbols, weights):
        """
        

        Parameters
        ----------
        df_portfolio : TYPE
            DESCRIPTION.
        enter : TYPE
            DESCRIPTION.
        list_symbols : TYPE
            DESCRIPTION.
        weights : TYPE
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        profit_lost=pd.DataFrame()
        for row in range(len(enter)):
            try:
                test=pd.DataFrame()
                df_filter_onEntryDate=df_portfolio.loc[df_portfolio.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
                for security in list_symbols:
                    df = df_filter_onEntryDate.loc[df_filter_onEntryDate.ticker==security]
                    test[security] =df.best_bid.tolist()
                    test.index=df.date
                test= test.replace(0, 0.01)
                returns_df= test.pct_change()
                returns_df= returns_df.iloc[0: , :]
                returns_df= returns_df.fillna(0.01)
                returns_df= returns_df*weights.weights.tolist()
                returns_df= returns_df.sum(axis=1)
                profit_lost=pd.concat([profit_lost,returns_df])
            except:
                print(f"error in data with entry {enter.iloc[row].name}, missing data of: {security}")
        return profit_lost.groupby(profit_lost.index).mean()

    def delta_hedged_returns(self, df_portfolio, enter, list_symbols, weights, df_price):
        profit_lost=pd.DataFrame()
        for row in range(len(enter)):
            try:
                test=pd.DataFrame()
                df_filter_onEntryDate=df_portfolio.loc[df_portfolio.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
                for security in list_symbols:
                    df = df_filter_onEntryDate.loc[df_filter_onEntryDate.ticker==security]
                    test[security] =df.best_bid.tolist()
                    test.index=df.date
                test= test.replace(0, 0.01)
                returns_df= test.pct_change()
                returns_df= returns_df.iloc[0: , :]
                returns_df= returns_df.fillna(0.01)
                returns_df= returns_df*weights.weights.tolist()
                returns_df= returns_df.sum(axis=1)
                profit_lost=pd.concat([profit_lost,returns_df])
            except:
                print(f"error in data with entry {enter.iloc[row].name}, missing data of: {security}")
        return profit_lost.groupby(profit_lost.index).mean()
    
    def returns_portfolio_delta(self,df_portfolio, enter, list_symbols,weights,df_price):
        profit_lost=pd.DataFrame()
        
    
        for row in range(len(enter)):
            df_options_price=pd.DataFrame()
            df_equity_price=pd.DataFrame()
            df_delta=pd.DataFrame()
            df_equity_price=pd.DataFrame()
            price=pd.DataFrame()
            try:
                df_filter_onEntryDate=df_portfolio.loc[df_portfolio.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
                for security in list_symbols:
                    
                    #get data per ticker and entery moment
                    df = df_filter_onEntryDate.loc[df_filter_onEntryDate.ticker==security]
    
                    #get option prices
                    df_options_price[security]=df.best_bid
                    df_options_price.index=df.date
    
                    #get the delta position
                    df_delta[security]=df.delta
                    df_delta.index=df.date
    
                    #get the price of the underlying asset 
                    df_equity_price=df_price[security]
                    df_equity_price=df_equity_price.loc[df_equity_price.index>=(df_filter_onEntryDate.date.min())]
                    df_equity_price=df_equity_price.loc[df_equity_price.index<=df_filter_onEntryDate.date.max()]
                    price[security]=df_equity_price
                    price.index=df_equity_price.index
    
                #caclulate options returns
                df_options_price=df_options_price.replace(0, 0.01)
                returns_df=df_options_price.pct_change()
                returns_df=returns_df.iloc[0:,:]
                returns_df=returns_df.fillna(0)
                returns_df=returns_df*weights.weights.tolist()
                returns_df=returns_df.mean(axis=1)
    
                #calculate delta hedging returns
                price=price.replace(0,0.01)
                df_equity_returns=price.pct_change()
                df_delta_returns=df_equity_returns*df_delta
                df_delta_returns=df_delta_returns.iloc[0:,:]
                df_delta_returns=df_delta_returns.fillna(0)
                df_delta_returns=df_delta_returns*weights.weights.tolist()
                df_delta_returns=df_delta_returns.mean(axis=1)
                profit_lost=pd.concat([profit_lost,returns_df,df_delta_returns])
            except:
                print(f"error in data with entry {enter.iloc[row].name}, missing data of: {security}")
            
        return profit_lost.groupby(profit_lost.index).mean()
    
    def trade_book_straddle(self,enter, option_data, df_weights, df_price,date_lower_bound, date_higher_bound,list_symbols):
        trade_book_straddle = pd.DataFrame()
        for row in range(len(enter)):
            for ticker in list_symbols:
                option_data_ticker = option_data[option_data.ticker==ticker] #get data of the ticker
                close_price_group = df_price.loc[enter.iloc[row].name] #get the close price of the group of asset for the strike price 
                close_price = close_price_group.loc[close_price_group.index == ticker] #get the close price for the individual asset            
                df_filtered_date=option_data_ticker.loc[option_data_ticker.date==enter.iloc[row].name]
                df_filtered_strike=df_filtered_date.loc[df_filtered_date.strike_price==int(round(close_price,-1)*1000)]
                df_filtered_maturity=df_filtered_strike.loc[(df_filtered_strike.exdate>enter.iloc[row].name+timedelta(days=date_lower_bound)) ]
        
                #strategy for the index
                if ticker == 'OEX':
                    if enter.iloc[row].trade == 'Short':
                        #Short dispersion is buy OEX straddles
                        df_filtered_cp=df_filtered_maturity
                        df_filtered_cp['straddle_weights']=1
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short                    
                        trade_book_straddle=pd.concat([df_filtered_cp,trade_book_straddle],ignore_index=True)
        
        
                    if enter.iloc[row].trade == 'Long':
                        #long dispersion is selling OEX straddles
                        df_filtered_cp=df_filtered_maturity
                        df_filtered_cp['straddle_weights']=-1
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_straddle=pd.concat([df_filtered_cp,trade_book_straddle],ignore_index=True)
        
                else:
                    if enter.iloc[row].trade == 'Short':
                        #Short dispersion is buy consituents straddles
                        df_filtered_cp=df_filtered_maturity
                        df_filtered_cp['straddle_weights']=-1
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_straddle=pd.concat([df_filtered_cp,trade_book_straddle],ignore_index=True)
        
        
                    if enter.iloc[row].trade == 'Long':
                        #long dispersion is selling constituents straddles
                        df_filtered_cp=df_filtered_maturity
                        df_filtered_cp['straddle_weights']=1
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_straddle=pd.concat([df_filtered_cp,trade_book_straddle],ignore_index=True)
        
        
        return trade_book_straddle
    
    def returns_portfolio_straddle(self,df_portfolio_straddle, enter, list_symbols,weights):
        profit_lost=pd.DataFrame()
        for row in range(len(enter)):
            try:
                test=pd.DataFrame()
                df_filter_onEntryDate=df_portfolio_straddle.loc[df_portfolio_straddle.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
                for security in list_symbols:
                    df = df_filter_onEntryDate.loc[df_filter_onEntryDate.ticker==security]
                    test[security]=(df.best_bid*df.straddle_weights).values            
                test.index=df.date
                test=test.groupby(test.index).mean()
                test= test.replace(0, 0.01)
                returns_df= test.pct_change()
                returns_df= returns_df.iloc[0:,:]
                returns_df= returns_df.fillna(0)
                returns_df= returns_df*weights.weights.tolist()
                returns_df= returns_df.mean(axis=1)
                profit_lost=pd.concat([profit_lost,returns_df])
            except:
                print(f"error in data with entry {enter.iloc[row].name}, missing data of: {security}")
        return profit_lost.groupby(profit_lost.index).mean()
    
    def exit_trade(self,trade_book1,enter):
        """
        

        Parameters
        ----------
        trade_book1 : TYPE
            DESCRIPTION.
        enter : TYPE
            DESCRIPTION.

        Returns
        -------
        trade_book1 : TYPE
            DESCRIPTION.

        """
        change=[]
        df_exit_date=pd.DataFrame()
        _=[]
        for row in range(len(enter)):
            try:
                if enter.iloc[row].trade != enter.iloc[row+1].trade:
                    change.append(enter.iloc[row+1].name)
            except:
                print('End of Loop')
                
    
        i=0
        exit_date=[]
        try:
            for row in range(len(enter)):
                exit_date.append(change[i])
                for row2 in range(len(change)):
                    if enter.iloc[row].name==change[row2]:
                        i+=1
    
        except:
            print()
        exit_date.pop(0)
        difference = len(enter)-len(exit_date)
        for _ in range(difference):
            exit_date.append(np.nan)
    
        df_exit_date=enter
        df_exit_date['exit_date']=exit_date 
        _=[]
        for row in range(len(trade_book1)):
            for row_1 in range(len(df_exit_date)):
                if trade_book1.date.loc[row] == df_exit_date.iloc[row_1].name:
                    _.append(df_exit_date.iloc[row_1].exit_date)
        trade_book1['exit_date_opposite_trade']=_    
        trade_book1['exit_date_maturity']=trade_book1.exdate-timedelta(days=7)
        
        list_exit=[]
        for row in range(len(trade_book1)):
            list_exit.append(min(trade_book1.loc[row].exit_date_maturity,trade_book1.loc[row].exit_date_opposite_trade))
        trade_book1['exit_date']=list_exit
        trade_book1['bid_ask_spread']=trade_book1.best_offer-trade_book1.best_bid
        trade_book1.to_csv('../../Data/trades.csv')
    
        return trade_book1
    
    def portfolio(self,trade_book1,option_data):
        """
        

        Parameters
        ----------
        trade_book1 : TYPE
            DESCRIPTION.

        Returns
        -------
        df_portfolio : TYPE
            DESCRIPTION.

        """
        df_portfolio = pd.DataFrame()
        for row in range(len(trade_book1)):
            optionid = trade_book1.optionid.loc[row]
            begin_date = trade_book1.date.loc[row]
            end_date = trade_book1.exit_date.loc[row]
            df=option_data.loc[option_data.optionid==optionid]
            df=df.loc[option_data.date>begin_date]
            df=df.loc[df.date<end_date]
            df['EntryDate']=begin_date
            df_portfolio= pd.concat([df_portfolio,df])
    
        return df_portfolio
    
    def filter_highest_maturity(self,df_trade_book,enter):
        trade_book1 = pd.DataFrame()
        for row in range(len(enter)):
            trade_book_perentry=df_trade_book.loc[df_trade_book.date==enter.iloc[row].name]
            highest_date=trade_book_perentry.exdate.max()
            df=trade_book_perentry.loc[trade_book_perentry.exdate==highest_date]
            _ = df.drop_duplicates(subset=['optionid'])
            trade_book1=pd.concat([trade_book1,_],ignore_index=True)
        return trade_book1
    
    def filter_maturity(self,trade_book_plain,enter, list_symbols):
        """
        

        Parameters
        ----------
        trade_book_plain : TYPE
            DESCRIPTION.
        enter : TYPE
            DESCRIPTION.

        Returns
        -------
        trade_book : TYPE
            DESCRIPTION.

        """
        trade_book = pd.DataFrame()
        for row in range(len(enter)):
            trade_book_perentry = trade_book_plain.loc[trade_book_plain.date==enter.iloc[row].name]
            for date in trade_book_perentry.exdate.tolist():
                if len(trade_book_perentry.ticker.loc[trade_book_perentry.exdate==date])==len(list_symbols):
                    _ = trade_book_perentry.loc[trade_book_perentry.exdate==date]
                    trade_book = pd.concat([trade_book,_],ignore_index=True)
        return trade_book
  
    def missing_values(self,enter, df_portfolio,list_symbols):
      missing_values={'security': [], 'enter': [], 'date':[]}
      for row in range(len(enter)):
          df=df_portfolio.loc[df_portfolio.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
          for date in df.date.unique():
              df=df_portfolio.loc[df_portfolio.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
              df=df.loc[df.date==date]
              if len(df.ticker.tolist())!=len(list_symbols):
                  for security in list_symbols:
                      if security not in df.ticker.tolist():
                          missing_values['security'].append(security)
                          missing_values['enter'].append(enter.iloc[row].name)
                          missing_values['date'].append(date)
      return pd.DataFrame(missing_values)  
  
    def filter_maturity_straddle(self,trade_book_plain,enter, list_symbols):
        """
        
    
        Parameters
        ----------
        trade_book_plain : TYPE
            DESCRIPTION.
        enter : TYPE
            DESCRIPTION.
    
        Returns
        -------
        trade_book : TYPE
            DESCRIPTION.
    
        """
        trade_book = pd.DataFrame()
        for row in range(len(enter)):
            trade_book_perentry = trade_book_plain.loc[trade_book_plain.date==enter.iloc[row].name]
            for date in trade_book_perentry.exdate.tolist():
                if len(trade_book_perentry.ticker.loc[(trade_book_perentry.exdate==date) & (trade_book_perentry.cp_flag=='C')])==len(list_symbols) and len(trade_book_perentry.ticker.loc[(trade_book_perentry.exdate==date) & (trade_book_perentry.cp_flag=='P')])==len(list_symbols) :
                    _ = trade_book_perentry.loc[trade_book_perentry.exdate==date]
                    trade_book = pd.concat([trade_book,_],ignore_index=True)
        return trade_book
    
    def portfolio_straddle(self,trade_book1,option_data):
        """
        

        Parameters
        ----------
        trade_book1 : TYPE
            DESCRIPTION.

        Returns
        -------
        df_portfolio : TYPE
            DESCRIPTION.

        """
        df_portfolio = pd.DataFrame()
        for row in range(len(trade_book1)):
            straddle_weights = trade_book1.straddle_weights.loc[row]
            optionid = trade_book1.optionid.loc[row]
            begin_date = trade_book1.date.loc[row]
            end_date = trade_book1.exit_date.loc[row]
            df=option_data.loc[option_data.optionid==optionid]
            df=df.loc[option_data.date>begin_date]
            df=df.loc[df.date<end_date]
            df['EntryDate']=begin_date
            df['straddle_weights']=straddle_weights
            df_portfolio= pd.concat([df_portfolio,df])
    
        return df_portfolio

class VolatilityCorrelation:
    """
    The class VolatilityCorrelation calculates the volatility of the index & the tracking portfolio.
       
    """
    
    def dispersion(self, oex_imv, constituents_imv, weights):
        """
        This function calculates the dispers 

        Parameters
        ----------
        oex_imv : TYPE
            Index implied volatility.
        constituents_imv : TYPE
            individual assets implied volatility.
        weights : weights of the individual assets
            DESCRIPTION.

        Returns
        -------
        dispersion : TYPE
            DESCRIPTION.

        """
        oex_imv=oex_imv.squeeze()
        weighted_imv_oex = (constituents_imv*weights).sum(axis=1)
        dispersion = weighted_imv_oex**2-oex_imv**2
        return dispersion
        

    def implied_correlation(self,index_ivol,portfolio_ivol,weights):
        """
        This function calculates the implied correlation
    
        Parameters
        ----------
        index_ivol : pandas dataframe
            Implied volatility of the index.
        portfolio_ivol : pandas dataframe
            Implied volatility of the individual assets.
        weights : pandas series
            Weights of the portfolio.
    
        Returns
        -------
        implied_cor : Pandas Series
            30 Day Implied correlation.
    
        """
        weights=np.array(weights)
        wi = weights
        wj = weights.T
        wi = wi.tolist()
        wj = wj.tolist()
        sigma_i = portfolio_ivol
        df_implied_corr=pd.DataFrame()
        try:
            len(sigma_i.index)==len(index_ivol.index)
        except:
            print("Dataframes do not align in dates")
        for row in range(len(sigma_i)):
            sigma_i_row = sigma_i.iloc[row]#iterate over sigma_i
            sigma_j_row = sigma_i_row.transpose()#get sigma_j, the transpose of sigma_i 
            index_ivol_row = index_ivol.iloc[row]#interate over the ivol of the index
            pairwise_weighted_iv = (np.dot(wi,np.dot(wj,np.dot(sigma_i_row,sigma_j_row)))).sum()
            pairwise_weighted_iv = 2*pairwise_weighted_iv
            index_variance = (index_ivol_row**2)
            portfolio_variance = np.dot(((pd.Series(wi))**2),(sigma_i_row**2))
            implied_corr = (index_variance - portfolio_variance)/pairwise_weighted_iv
            df_temp = pd.DataFrame()
            df_temp['implied_corr']=implied_corr.values
            df_temp['Date']=implied_corr.name
            df_implied_corr=df_implied_corr.append(df_temp)
        return df_implied_corr


    def sample_correlation(self, returns_constituents, portfolio_ivol,weights):
        """
        This function calculates the sample correlation according to the formula
        given by Ferrari et al 2020        
    
        Parameters
        ----------
        returns_constituents : Pandas dataframe
            DESCRIPTION.
        portfolio_ivol : Pandas dataframe
            DESCRIPTION.
        weights : Pandas dataframe
            DESCRIPTION.
    
        Returns
        -------
        sample_correlation : Pandas dataserie
            DESCRIPTION.
    
        """
        corr = returns_constituents.rolling(30).corr(pairwise=True).dropna()
        corr = corr.loc[portfolio_ivol.index[0]:portfolio_ivol.index[-1]]
        weights=np.array(weights)
        wi = weights
        wj = weights.T
        wi = wi.tolist()
        wj = wj.tolist()
        sigma_i = portfolio_ivol
        df_implied_corr_sample=pd.DataFrame()
        try:
            len(sigma_i.index)==len(corr.index)
        except:
            print("Dataframes do not align in dates")
        for row in range(len(sigma_i)):
            sigma_i_row = sigma_i.iloc[row]#iterate over sigma_i
            sigma_j_row = sigma_i_row.transpose()#get sigma_j, the transpose of sigma_i 
            date = sigma_i.iloc[row].name
            corr_row = corr.loc[date]
            numerator=(np.dot(wi,np.dot(wj,np.dot(sigma_i_row,np.dot(sigma_j_row,corr_row))))).sum()
            denomirator=(np.dot(wi,np.dot(wj,np.dot(sigma_i_row,sigma_j_row)))).sum()
            sample_correlation=numerator/denomirator
            df_temp = pd.DataFrame()
            df_temp['implied_corr']=pd.Series(sample_correlation)
            df_temp['Date']=pd.Series(date)
            df_implied_corr_sample=df_implied_corr_sample.append(df_temp)
            
        return df_implied_corr_sample
        
        def indicator(self, implied_correlation, sample_correlation):
            """
            

            Parameters
            ----------
            implied_correlation : DataFrame
                DESCRIPTION.
            sample_correlation : DataFrame
                DESCRIPTION.

            Returns
            -------
            Dataframe
                Dispersion Trading entry indicator.

            """
            indicator = implied_correlation - sample_correlation
            return indicator

          