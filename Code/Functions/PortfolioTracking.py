# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 22:47:33 2022

@author: ajzwa
"""
from sklearn.decomposition import PCA
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from scipy import stats

class PortfolioTracking:
    """
    the class PortfolioTracking mimics an index with a N number of its components
       
    Attributes
    ----------
        returns components
        returns index

    Methods
    ----------
        weights_df: gives a dataframe with weights of the tracking portfolio
    """
    def pca_plot(self):
        """
        calculates the principle component analysis of the covariance matrix of the index components
        
        Parameters
        -----------
        n_components, data
          
        Returns
        --------
        scatter plat with the pca analysis including the sector
        """
        
        #first load the data
        p_components = pd.read_csv("../Data/price/price_components.csv",index_col=0).dropna()
        meta_data = pd.read_csv("../data/components/metadata.csv")
        #calculate the returns from the price
        returns_components = p_components.pct_change().dropna()
        #apply a 2 dimensional pca in order to able to plot a 2 dimensional graph with the results
        n_components=2  
        #perform the PCA
        X = returns_components.transpose()
        pca = PCA(n_components=n_components)
        components = pca.fit_transform(X) 
        df_components = pd.DataFrame(data=components,columns=['x', 'y'])
        #add meta data regarding the sector and marketCap
        df_components['sector'] = meta_data.sector
        df_components['MarketCap'] = meta_data.marketCap
        #group by sector in order to including the sectors in the graph
        groups = df_components.groupby('sector')
        # Plot
        fig, ax = plt.subplots()
        for name, group in groups:
            ax.plot(group.x, group.y, marker='o', linestyle='', ms=12, label=name, alpha=0.75)
        ax.legend(bbox_to_anchor=(1.05, 1))
        plt.title("Principle Component Analysis AEX")
        plt.savefig("../Graphs/pca_aex.png", bbox_extra_artists=(ax,), bbox_inches='tight')
        return plt
    
    def pca_tracking_weights(self):
        """
        calculates the weights for every component in order to track the index
        
        Parameters
        -----------

          
        Returns
        --------
        DataFrame with the name & weights
        """
        #import the data
        data = pd.read_csv("../Data/price/price_components.csv", index_col=0)
        meta_data = pd.read_csv("../Data/components/metadata.csv")
        returns_components = data.pct_change().dropna() 
        #compute the pca analysis
        X = returns_components.transpose()
        pca = PCA(n_components=2)
        components = pca.fit_transform(X)
        df_components = pd.DataFrame(data=components,columns=['x', 'y'])
        df_components["Name"]=meta_data.shortName
        #calculate the weights with the help of the explained variance
        weights = abs(df_components['x'])/sum(abs(df_components['x']))
        df_components['weights'] = weights.tolist()
        return df_components[['Name',"weights"]]

        
    def pca_tracking_plot(self):
        """
        plots the tracking index and index and calculates the MSE
        
        Parameters
        -----------

          
        Returns
        --------
        mean squared error of the index and tracking index & plot with the returns of both the index & tracking index
        """
        #import the data
        data = pd.read_csv("../Data/price/price_components.csv", index_col=0)
        meta_data = pd.read_csv("../Data/components/metadata.csv")
        p_index = pd.read_csv("../Data/price/price_index.csv", index_col=0)
        #calculate the returns
        returns_components = data.pct_change().dropna() 
        returns_index=p_index.pct_change().dropna()
        #perfrom the pca
        X = returns_components.transpose()
        pca = PCA(n_components=2)
        components = pca.fit_transform(X)
        df_components = pd.DataFrame(data=components,columns=['x', 'y'])
        df_components["Name"]=meta_data.shortName
        #calculate the weights
        weights = abs(df_components['x'])/sum(abs(df_components['x']))
        df_components['weights'] = weights.tolist()
        tracking_index = weights.tolist()*returns_components
        portfolio_index = tracking_index.sum(axis=1)
        
        index_returns = returns_index[returns_index.index.isin(portfolio_index.index)]
        
        fig, ax = plt.subplots(figsize=(12,6))
        plt.title("Returns Tracking Index")
        ax.plot(portfolio_index,label='tracking portfolio')
        ax.plot(index_returns,label='index')
        ax.xaxis.set_ticklabels([])
        ax.legend() 
        #calculated MSE & t-test
        mse = mean_squared_error(index_returns,portfolio_index)
        plt.savefig("../Graphs/portfolio_index_returns.png")
        return(print("Mean Sqaured error:",mse),plt.show())

    def cum_returns(self):
        """
        
        
        Parameters
        -----------

          
        Returns
        --------
        
        """
        #import the data
        data = pd.read_csv("../Data/price/price_components.csv", index_col=0)
        meta_data = pd.read_csv("../Data/components/metadata.csv")
        p_index = pd.read_csv("../Data/price/price_index.csv", index_col=0)
        #calculate the returns
        returns_components = data.pct_change().dropna() 
        returns_index=p_index.pct_change().dropna()
        #perfrom the pca
        X = returns_components.transpose()
        pca = PCA(n_components=2)
        components = pca.fit_transform(X)
        df_components = pd.DataFrame(data=components,columns=['x', 'y'])
        df_components["Name"]=meta_data.shortName
        #calculate the weights
        weights = abs(df_components['x'])/sum(abs(df_components['x']))
        df_components['weights'] = weights.tolist()
        tracking_index = weights.tolist()*returns_components
        portfolio_index = tracking_index.sum(axis=1)
        
        index_returns = returns_index[returns_index.index.isin(portfolio_index.index)]
        cum_index = (index_returns +1).cumprod()
        cum_portfolio = (portfolio_index+1).cumprod()
        
        fig, ax = plt.subplots(figsize=(12,6))
        plt.title("Cumulative Returns Tracking Portfolio & Index")
        ax.plot(cum_portfolio,label='tracking portfolio')
        ax.plot(cum_index,label='index')
        ax.xaxis.set_ticklabels([])
        ax.legend()     
        plt.savefig("../Graphs/cum_returns_index_portfolio.png")
        t_test  = stats.ttest_ind(cum_index,cum_portfolio)

        return plt.show(), print("T-test",t_test)