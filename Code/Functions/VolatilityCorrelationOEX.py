# -*- coding: utf-8 -*-
"""
Created on Thu Feb 24 14:17:04 2022

@author: ajzwa


"""
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import numpy as np
import matplotlib.dates as mdates
import datetime

class VolatilityCorrelation:
    """
    The class VolatilityCorrelation calculates the volatility of the index & the tracking portfolio.
       
    """
    
    def dispersion(self, oex_imv, constituents_imv, weights):
        """
        This function calculates the dispers 

        Parameters
        ----------
        oex_imv : TYPE
            Index implied volatility.
        constituents_imv : TYPE
            individual assets implied volatility.
        weights : weights of the individual assets
            DESCRIPTION.

        Returns
        -------
        dispersion : TYPE
            DESCRIPTION.

        """
        oex_imv=oex_imv.squeeze()
        weighted_imv_oex = (constituents_imv*weights).sum(axis=1)
        dispersion = weighted_imv_oex**2-oex_imv**2
        return dispersion
        

    def implied_correlation(self,index_ivol,portfolio_ivol,weights):
        """
        This function calculates the implied correlation
    
        Parameters
        ----------
        index_ivol : pandas dataframe
            Implied volatility of the index.
        portfolio_ivol : pandas dataframe
            Implied volatility of the individual assets.
        weights : pandas series
            Weights of the portfolio.
    
        Returns
        -------
        implied_cor : Pandas Series
            30 Day Implied correlation.
    
        """
        weights=np.array(weights)
        wi = weights
        wj = weights.T
        wi = wi.tolist()
        wj = wj.tolist()
        sigma_i = portfolio_ivol
        df_implied_corr=pd.DataFrame()
        try:
            len(sigma_i.index)==len(index_ivol.index)
        except:
            print("Dataframes do not align in dates")
        for row in range(len(sigma_i)):
            sigma_i_row = sigma_i.iloc[row]#iterate over sigma_i
            sigma_j_row = sigma_i_row.transpose()#get sigma_j, the transpose of sigma_i 
            index_ivol_row = index_ivol.iloc[row]#interate over the ivol of the index
            pairwise_weighted_iv = (np.dot(wi,np.dot(wj,np.dot(sigma_i_row,sigma_j_row)))).sum()
            pairwise_weighted_iv = 2*pairwise_weighted_iv
            index_variance = (index_ivol_row**2)
            portfolio_variance = np.dot(((pd.Series(wi))**2),(sigma_i_row**2))
            implied_corr = (index_variance - portfolio_variance)/pairwise_weighted_iv
            df_temp = pd.DataFrame()
            df_temp['implied_corr']=implied_corr.values
            df_temp['Date']=implied_corr.name
            df_implied_corr=df_implied_corr.append(df_temp)
        return df_implied_corr


    def sample_correlation(self, returns_constituents, portfolio_ivol,weights):
        """
        This function calculates the sample correlation according to the formula
        given by Ferrari et al 2020        
    
        Parameters
        ----------
        returns_constituents : Pandas dataframe
            DESCRIPTION.
        portfolio_ivol : Pandas dataframe
            DESCRIPTION.
        weights : Pandas dataframe
            DESCRIPTION.
    
        Returns
        -------
        sample_correlation : Pandas dataserie
            DESCRIPTION.
    
        """
        corr = returns_constituents.rolling(30).corr(pairwise=True).dropna()
        corr = corr.loc[portfolio_ivol.index[0]:portfolio_ivol.index[-1]]
        weights=np.array(weights)
        wi = weights
        wj = weights.T
        wi = wi.tolist()
        wj = wj.tolist()
        sigma_i = portfolio_ivol
        df_implied_corr_sample=pd.DataFrame()
        try:
            len(sigma_i.index)==len(corr.index)
        except:
            print("Dataframes do not align in dates")
        for row in range(len(sigma_i)):
            sigma_i_row = sigma_i.iloc[row]#iterate over sigma_i
            sigma_j_row = sigma_i_row.transpose()#get sigma_j, the transpose of sigma_i 
            date = sigma_i.iloc[row].name
            corr_row = corr.loc[date]
            numerator=(np.dot(wi,np.dot(wj,np.dot(sigma_i_row,np.dot(sigma_j_row,corr_row))))).sum()
            denomirator=(np.dot(wi,np.dot(wj,np.dot(sigma_i_row,sigma_j_row)))).sum()
            sample_correlation=numerator/denomirator
            df_temp = pd.DataFrame()
            df_temp['implied_corr']=pd.Series(sample_correlation)
            df_temp['Date']=pd.Series(date)
            df_implied_corr_sample=df_implied_corr_sample.append(df_temp)
            
        return df_implied_corr_sample
        
        def indicator(self, implied_correlation, sample_correlation):
            """
            

            Parameters
            ----------
            implied_correlation : DataFrame
                DESCRIPTION.
            sample_correlation : DataFrame
                DESCRIPTION.

            Returns
            -------
            Dataframe
                Dispersion Trading entry indicator.

            """
            indicator = implied_correlation - sample_correlation
            return indicator


        